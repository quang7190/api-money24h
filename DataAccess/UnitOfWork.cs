﻿using System;
using System.Data;
using System.Data.SqlClient;
using DataAccess.Implementation;
using DataAccess.Repositories;
using Infrastructure;
namespace DataAccess
{
    public class UnitOfWork : IUnitOfWork
    {
        private IDbTransaction _transaction;
        private IDbConnection _connection;
        private bool _disposed;
        private IMoney24hRepository _money24h;

        public UnitOfWork(string connectionString)
        {
            _connection = new SqlConnection(connectionString);
            _connection.Open();
            _transaction = _connection.BeginTransaction();
            _disposed = false;
        }
        public IMoney24hRepository money24hRepository
        {
            get
            {
                if (_money24h == null)
                {
                    _money24h = new Money24hRepository(_transaction);
                }
                return _money24h;
            }
        }
        public void Commit()
        {
            try
            {
                _transaction.Commit();
            }
            catch
            {
                _transaction.Rollback();
                throw;
            }
            finally
            {
                _connection.Dispose();
                _connection.Close();
                _transaction.Dispose();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (_transaction != null)
                {
                    _transaction.Dispose();
                    _transaction = null;
                }

                if (_connection != null)
                {
                    _connection.Dispose();
                    _connection = null;
                }

                _disposed = true;
            }
        }

    }
}
