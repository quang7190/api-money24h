﻿using DataAccess.Repositories;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using static Dapper.SqlMapper;

namespace DataAccess.Implementation
{
    public class Money24hRepository : BaseRepository, IMoney24hRepository
    {
        public Money24hRepository(IDbTransaction transaction)
          : base(transaction)
        {
        }
        public async Task<IEnumerable<ListBank>> GetListBank()
        {
            try
            {
                var result = await QueryAsync<ListBank>("SPA_GET_LIST_BANK", new
                {

                });
                return result;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<GridReader> GetAllReward()
        {
            try
            {
                var result = await QueryMultipleAsync("SPA_GET_REWARD_ALL", new
                {

                });
                return result;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
