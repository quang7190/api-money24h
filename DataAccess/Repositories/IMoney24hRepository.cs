﻿using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using static Dapper.SqlMapper;

namespace DataAccess.Repositories
{
    public interface IMoney24hRepository
    {
        Task<IEnumerable<ListBank>> GetListBank();
        Task<GridReader> GetAllReward();
    }
}
