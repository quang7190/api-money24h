﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.ViewModels
{
    public class ListBank
    {
        public int Id { get; set; }
        public string BankName { get; set; }
    }
}
