﻿using Models.BaseModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models.ViewModels
{
    public class AllReward
    {
        public List<Result> power655 { get; set; } = new List<Result>();
        public List<Result> mega645 { get; set; } = new List<Result>();
        public List<Result> max3d { get; set; } = new List<Result>();
        public List<Result> max4d { get; set; } = new List<Result>();
    }
}
