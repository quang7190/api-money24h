﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.BaseModels
{
    public class Result
    {
        public long Reward { get; set; }
        public string Name { get; set; }
    }
}
