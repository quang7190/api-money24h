﻿using Microsoft.AspNetCore.Mvc;
using Models.ViewModels;
using PublicApi.Models;
using Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ultilities;

namespace PublicApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class Money24hController : ControllerBase
    {
        private readonly ICacheHelper _cacheHelper;
        private readonly IMoney24hService _money24hService;
        public Money24hController(IMoney24hService money24hService, ICacheHelper cacheHelper)
        {
            _money24hService = money24hService;
            _cacheHelper = cacheHelper;
        }

        [HttpGet("get-list-bank")]
        public async Task<IActionResult> GetListBank()
        {
            try
            {
                var result = await _money24hService.GetListBank();
                if (!result.Any())
                {
                    return Ok(new BaseResponse<object>(null, "Data not found"));
                }
                return Ok(new BaseResponse<IEnumerable<ListBank>>(result));
            }
            catch (Exception ex)
            {
                return Ok(new BaseResponse<object>("Error", ex.Message));
            }
        }

        [HttpGet("get-all-reward")]
        public async Task<IActionResult> GetAllReward()
        {
            try
            {
                var result = await _money24hService.GetAllReward();
                if (result == null)
                {
                    return Ok(new BaseResponse<object>(null, "Data not found"));
                }
                return Ok(new BaseResponse<object>(result));
            }
            catch (Exception ex)
            {
                return Ok(new BaseResponse<object>("Error", ex.Message));
            }
        }
    }
}
