﻿using System;
namespace PublicApi.Models
{
    public class BaseResponse<T> where T : class
    {
        public T Data { get; set; }
        public bool Success { get; set; }
        public string Message { get; set; }
        public string ErrorCode { get; set; }
        public BaseResponse(T data)
        {
            Data = data;
            Success = true;
            Message = "Success";
            ErrorCode = "1";
        }
        public BaseResponse(string error, string message)
        {
            Data = null;
            Success = false;
            Message = message;
            ErrorCode = error;
        }
    }
}
