using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DataAccess;
using Infrastructure;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Serialization;
using Services.Implementation;
using Services.Services;
using Ultilities;
using static Dapper.SqlMapper;

namespace PublicApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IConfiguration>(Configuration);
            services.AddSingleton<IApplicationSettings, ApplicationSettings>();
            services.AddScoped<IMoney24hService, Money24hService>();
            services.AddScoped<ICacheHelper, CacheHelper>();

            bool isEnableRedis = Configuration.GetSection("RedisAuthen").GetValue<bool>("IsActive");
            if (isEnableRedis)
            {
                services.AddDistributedRedisCache(options =>
                {
                    options.Configuration = Configuration.GetSection("RedisAuthen").GetValue<string>("ConnectionString");
                    options.InstanceName = Configuration.GetSection("RedisAuthen").GetValue<string>("InstanceName");
                });
            }

            services.AddCors(options =>
            {
                options.AddDefaultPolicy(
                    builder =>
                    {
                        builder.WithOrigins("http://novaphanthiet.top/");
                    });
            });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1",
                    new Info
                    {
                        Title = "My API - V1",
                        Version = "v1"
                    }
                 );

                //var filePath = Path.Combine(System.AppContext.BaseDirectory, "MyApi.xml");
                //c.IncludeXmlComments(filePath);
            });

            services.AddRouting(options => options.LowercaseUrls = true);
            services.AddControllers().AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            });
            ApplicationSettingsFactory.InitializeApplicationSettings(new ApplicationSettings(Configuration));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });

            app.UseRouting();
            app.UseCors();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
    public static class ApplicationSettingsFactory
    {
        private static IApplicationSettings _iApplicationSetting;

        public static void InitializeApplicationSettings(IApplicationSettings iApplicationSettings)
        {

            _iApplicationSetting = iApplicationSettings;
            using (UnitOfWork uow = new UnitOfWork(iApplicationSettings.ConnectionString))
            {
                
                Dictionary<string, int> prizesKeyPairValue = new Dictionary<string, int>();
                Dictionary<string, int> lotteryTypesKeyPairValue = new Dictionary<string, int>();
                Dictionary<string, string> lotterySchedulesKeyPairValue = new Dictionary<string, string>();
                

                
                uow.Commit();
            }
        }
        public static IApplicationSettings GetApplicationSettings()
        {
            return _iApplicationSetting;
        }
    }
}
