﻿using Models.BaseModels;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.Services
{
    public interface IMoney24hService
    {
        Task<IEnumerable<ListBank>> GetListBank();
        Task<AllReward> GetAllReward();
    }
}
