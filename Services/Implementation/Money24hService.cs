﻿using DataAccess;
using Infrastructure;
using Models.BaseModels;
using Models.ViewModels;
using Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Implementation
{
    public class Money24hService : IMoney24hService
    {
        private readonly IApplicationSettings _application;
        private readonly string _connectString;
        private readonly Dictionary<string, int> _prizes;

        public Money24hService(IApplicationSettings application)
        {
            _application = application;
            _connectString = _application.ConnectionString;
        }

        public async Task<IEnumerable<ListBank>> GetListBank()
        {
            try
            {
                using (UnitOfWork uow = new UnitOfWork(_connectString))
                {
                    var result = await uow.money24hRepository.GetListBank();
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<AllReward> GetAllReward()
        {
            try
            {
                AllReward allReward = new AllReward();
                using (UnitOfWork uow = new UnitOfWork(_connectString))
                {
                    var result = await uow.money24hRepository.GetAllReward();

                    var power655= await result.ReadAsync<Result>();
                    var mega645 = await result.ReadAsync<Result>();
                    var max3D = await result.ReadAsync<Result>();
                    var max4D = await result.ReadAsync<Result>();

                    allReward.power655 = power655.ToList<Result>();
                    allReward.mega645 = mega645.ToList<Result>();
                    allReward.max3d = max3D.ToList<Result>();
                    allReward.max4d = max4D.ToList<Result>();
                    uow.Commit();
                }
                return allReward;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
